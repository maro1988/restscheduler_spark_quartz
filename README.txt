4567 is specific port for spark!

POST https://localhost:4567/jobs
{
  "id" : 1,
  "communicate" : "Hello World!",
  "forever" : true,
  "howOften" : 1,
  "timeUnit" : "SECOND"
}

POST https://localhost:4567/jobs
{
  "id" : 2,
  "communicate" : "2222222",
  "forever" : true,
  "howOften" : 2,
  "timeUnit" : "SECOND"
}

DELETE https://localhost:4567/jobs/1
{
  "id" : 1
}

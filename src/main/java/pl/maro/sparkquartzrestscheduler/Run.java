package pl.maro.sparkquartzrestscheduler;


import com.google.gson.Gson;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import pl.maro.sparkquartzrestscheduler.StandardResponse.StandardResponse;
import pl.maro.sparkquartzrestscheduler.StandardResponse.StatusResponse;
import pl.maro.sparkquartzrestscheduler.model.Job;
import pl.maro.sparkquartzrestscheduler.model.JobService;
import pl.maro.sparkquartzrestscheduler.model.JobServiceImpl;
import pl.maro.sparkquartzrestscheduler.quartz.SimpleJob;

import static org.quartz.JobBuilder.newJob;
import static spark.Spark.*;

public class Run {

    static JobService jobService = new JobServiceImpl();
    static SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    static Scheduler scheduler;



    public static void main(String[] args) {

        try {
            scheduler = schedulerFactory.getScheduler();
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        get("/jobs", (req, res) -> {
            res.type("application/json");
            return new Gson().toJson(
                    new StandardResponse(StatusResponse.SUCCESS, new Gson()
                            .toJsonTree(jobService.getJobs())));
        });

        post("jobs", (req, res) -> {
            res.type("application/json");
            Job job = new Gson().fromJson(req.body(), Job.class);
            scheduleJob(job);
            jobService.addJob(job);

            return new Gson()
                    .toJson(new StandardResponse(StatusResponse.SUCCESS));
        });

        delete("/jobs/:id", (req, res) -> {
            res.type("application/json");
            Job job = jobService.deleteJob(req.params(":id"));
            unscheduleJob(job);
            return new Gson().toJson(
                    new StandardResponse(StatusResponse.SUCCESS, "job deleted"));
        });
    }

    public static void scheduleJob(Job job){
        JobDetail jobDetail = newJob(SimpleJob.class)
                .withIdentity("Job"+job.getId(), "group")
                .usingJobData("communicate", job.getCommunicate())
                .build();

        Trigger trigger = getTrigger(job);

        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    private static Trigger getTrigger(Job job) {
        if(job.isForever()) {
            return getTriggerForever(job);
        } else {
            return getTriggerTemporary(job);
        }
    }

    private static Trigger getTriggerTemporary(Job job) {
        return TriggerBuilder.newTrigger()
                .withIdentity(job.getId(), "group")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(job.getHowOften()))
                .build();
    }

    private static Trigger getTriggerForever(Job job) {
        return TriggerBuilder.newTrigger()
                .withIdentity(job.getId(), "group")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(job.getHowOften())
                        .repeatForever())
                .build();
    }

    public static void unscheduleJob(Job job){
        Trigger trigger = getTrigger(job);
        try {
            scheduler.unscheduleJob(trigger.getKey());
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
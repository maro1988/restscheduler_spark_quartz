package pl.maro.sparkquartzrestscheduler.model;

import java.util.Collection;
import java.util.HashMap;

public class JobServiceImpl implements JobService {

    private HashMap<String, Job> map = new HashMap<>();

    @Override
    public void addJob(Job job) {
        map.put(job.getId() ,job);
    }

    @Override
    public Collection<Job> getJobs() {
        return map.values();
    }

    @Override
    public Job deleteJob(String id) {
        return map.remove(id);
    }
}

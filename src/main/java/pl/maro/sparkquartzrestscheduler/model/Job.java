package pl.maro.sparkquartzrestscheduler.model;



public class Job {
    private String id;
    private String communicate;
    private boolean forever;
    private int howOften;
    private TimeUnit timeUnit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommunicate() {
        return communicate;
    }

    public void setCommunicate(String communicate) {
        this.communicate = communicate;
    }

    public boolean isForever() {
        return forever;
    }

    public void setForever(boolean forever) {
        this.forever = forever;
    }

    public int getHowOften() {
        return howOften;
    }

    public void setHowOftencommunicate(int howOften) {
        this.howOften = howOften;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id='" + id + '\'' +
                ", communicate='" + communicate + '\'' +
                ", forever=" + forever +
                ", howOften=" + howOften +
                ", timeUnit=" + timeUnit +
                '}';
    }
}

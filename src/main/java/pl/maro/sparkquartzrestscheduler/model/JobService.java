package pl.maro.sparkquartzrestscheduler.model;

import java.util.Collection;

public interface JobService {

    public void addJob (Job job);

    public Collection<Job> getJobs ();

    public Job deleteJob (String id);

}
